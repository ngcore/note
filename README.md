# @ngcore/note
> NG Core angular/typescript note library


Client-side note components and services for Angular.
(Current version requires Angular v7.2+)


## API Docs

* http://ngcore.gitlab.io/note/




