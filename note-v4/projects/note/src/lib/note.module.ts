import { NgModule, ModuleWithProviders, ErrorHandler } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { NgCoreCoreModule } from '@ngcore/core';
import { NgCoreBaseModule } from '@ngcore/base';
import { NgCoreDataModule } from '@ngcore/data';
import { NgCoreIdleModule } from '@ngcore/idle';
import { NgCoreMarkModule } from '@ngcore/mark';

import { CommonNoteService } from './services/common-note-service';
import { UniversalNoteComponent } from './components/universal-note/universal-note.component';
import { UniversalAttachmentComponent } from './components/universal-attachment/universal-attachment.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgCoreCoreModule.forRoot(),
    NgCoreBaseModule.forRoot(),
    NgCoreDataModule.forRoot(),
    NgCoreIdleModule.forRoot(),
    NgCoreMarkModule.forRoot(),
  ],
  declarations: [
    UniversalNoteComponent,
    UniversalAttachmentComponent
  ],
  exports: [
    UniversalNoteComponent,
    UniversalAttachmentComponent
  ],
  entryComponents: [
    // ????
  ]
})
export class NgCoreNoteModule {
  static forRoot(): ModuleWithProviders<NgCoreNoteModule> {
    return {
      ngModule: NgCoreNoteModule,
      providers: [
        CommonNoteService
      ]
    };
  }
}
