import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { DateTimeUtil, DateIdUtil } from '@ngcore/core';


// Placeholder
@Injectable({
  providedIn : 'root'
})
export class CommonNoteService {

  constructor(
  ) {
    // console.log('Hello CommonNoteService Provider');
  }

  dummyFunction() {
  }

  // tbd:
  // ...

}
