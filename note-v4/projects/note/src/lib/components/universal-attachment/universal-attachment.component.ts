import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { DateIdUtil } from '@ngcore/core';
import { UniversalAttachment } from '@ngcore/data';
import { CommonTextEntryComponent, CommonMarkEntryComponent } from '@ngcore/mark';
import { MarkEvents } from '@ngcore/mark';
import { NoteEvents } from '../../common/events/note-events';


@Component({
  selector: 'common-universal-attachment',
  templateUrl: './universal-attachment.component.html',
  styleUrls: ['./universal-attachment.component.css']
})
export class UniversalAttachmentComponent implements OnInit {

  // temporary
  title: string;
  public data: UniversalAttachment;

  constructor() { 

    // temporary
    this.data = new UniversalAttachment();
    // ....

    // tbd....
    this.title = '(Title)';

  }

  ngOnInit() {
  }

  // tbd
  refreshData(_data: UniversalAttachment): void {
    if(isDL()) dl.log('UniversalAttachmentComponent: >>> refreshData() _data = ' + _data);

    if (_data) {
      this.data = _data.clone();  // ??
    } else {
      // ????
    }
    // tbd....
    if(this.data.title) {
      this.title = this.data.title;
    } else {
      // ???
      // let todayId = this.localUserService.todayId;
      // this.title = DateIdUtil.getLocaleDateString(todayId);
    }

    // if(isDL()) dl.log('>>> refreshData() this.data = ' + this.data);
  }


  saveAttachment(): void {
    // if(isDL()) dl.log('saveAttachment()');

    // // ???
    // this.events.publish(NoteEvents.MVP0_DATA_UNIVERSALATTACHMENT_UPDATE);
  }


}
