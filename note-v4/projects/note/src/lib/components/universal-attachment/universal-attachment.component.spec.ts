import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UniversalAttachmentComponent } from './universal-attachment.component';

describe('UniversalAttachmentComponent', () => {
  let component: UniversalAttachmentComponent;
  let fixture: ComponentFixture<UniversalAttachmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UniversalAttachmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UniversalAttachmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
