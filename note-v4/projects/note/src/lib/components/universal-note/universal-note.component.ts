import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { DateIdUtil } from '@ngcore/core';
import { UniversalNote } from '@ngcore/data';
import { CommonTextEntryComponent, CommonMarkEntryComponent } from '@ngcore/mark';
import { MarkEvents } from '@ngcore/mark';
import { NoteEvents } from '../../common/events/note-events';


@Component({
  selector: 'common-universal-note',
  templateUrl: './universal-note.component.html',
  styleUrls: ['./universal-note.component.css']
})
export class UniversalNoteComponent implements OnInit, OnDestroy {

  @ViewChild("commonTextEntry", { static: true }) commonTextEntry: CommonTextEntryComponent;
  @ViewChild("commonMarkEntry", { static: true }) commonMarkEntry: CommonMarkEntryComponent;

  // temporary
  // title: string;
  data: UniversalNote;

  _isShowHTML: boolean = false;
  get isShowHTML(): boolean {
    return this._isShowHTML;
  }
  set isShowHTML(_isShowHTML: boolean) {
    this._isShowHTML = _isShowHTML;
    // tbd: refresh the screen????
  }
  flipMarkdownHTMLView(event) {
    this.isShowHTML = !this._isShowHTML;
  }


  private _textChangedEventHandler: (newText: string) => void;

  constructor() {
    this._textChangedEventHandler = (newText: string) => {
      this.udpateMarkdownText(newText);
    }

    // temporary
    this.data = new UniversalNote();
    // this.data.userId = this.localUserService.currentUserId;
    // ....

    // tbd....
    // this.title = '(Title)';
  }

  ngOnInit() {
    // // if(isDL()) dl.log('::: ngOnInit()');
    // this.events.subscribe(MarkEvents.MARK_TEXT_ENTRY_TEXT_CHANGED, this._textChangedEventHandler);
  }
  ngOnDestroy() {
    // // if(isDL()) dl.log('::: ngOnDestroy()');
    // this.events.unsubscribe(MarkEvents.MARK_TEXT_ENTRY_TEXT_CHANGED, this._textChangedEventHandler);
  }

  titleChanged(newTitle) {
    if(isDL()) dl.log('0UniversalNoteComponent: titleChanged(): newTitle = ' + newTitle);
    this.data.setTitle(newTitle);
  }

  udpateMarkdownText(newText) {
    // if(isDL()) dl.log('udpateMarkdownText(): newText = ' + newText);

    this.data.setMarkdownText(newText);

    // Not needed, obviously.
    // this.commonTextEntry.setTextInput(newText);

    let showDebugLog = false;
    this.commonMarkEntry.setMarkdownInput(newText, null, showDebugLog);
  }

  // tbd
  refreshData(_data: UniversalNote): void {
    if(isDL()) dl.log('UniversalNoteComponent: >>> refreshData() _data = ' + _data);

    if (_data) {
      this.data = _data.clone();  // ??
    } else {
      // ????
    }
    // tbd....
    // if (this.data.title) {
    //   this.title = this.data.title;
    // } else {
    //   // ????
    //   // let todayId = this.localUserService.todayId;
    //   // this.title = DateIdUtil.getLocaleDateString(todayId);
    //   this.title = '';
    // }

    // ????
    if (!this.data.markdownText) {
      this.data.markdownText = '';
    }
    this.commonTextEntry.setTextInput(this.data.markdownText);
    this.commonMarkEntry.setMarkdownInput(this.data.markdownText);

    // if(isDL()) dl.log('>>> refreshData() this.data = ' + this.data);
  }


  saveNote(): void {
    // if(isDL()) dl.log('saveNote()');
    // // if(isDL()) dl.log('%%%%%%%% saveNote(): this.data = ' + this.data);

    // ???
    if (this.data.isSynched) {
      // this.events.publish(NoteEvents.NOTE_UNIVERSALNOTE_UPDATE, this.data);
    } else {
      // this.events.publish(NoteEvents.NOTE_UNIVERSALNOTE_CREATE, this.data);
    }
    // this.events.publish(CommonEvents.MVP0_NAV_DISMISS_SELF, true);

    // tbd:
    // This should be really done by the parent, but just as a shortcut...
    //   (note: this should not be necessary if we dismiss the dialog after saving...)
    this.data.isDirty = false;   // This can be a problem, if saving fails in the parent page....
    // ...
  }


}
