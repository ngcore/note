import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UniversalNoteComponent } from './universal-note.component';

describe('UniversalNoteComponent', () => {
  let component: UniversalNoteComponent;
  let fixture: ComponentFixture<UniversalNoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UniversalNoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UniversalNoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
